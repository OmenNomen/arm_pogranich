unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls,
  ZAbstractConnection, ZConnection, ZAbstractRODataset, ZAbstractDataset,
  ZDataset, Datasnap.Provider, Datasnap.DBClient, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.DBCtrls;

type
  TForm2 = class(TForm)
    DBGrid1: TDBGrid;
    ClientDataSet1: TClientDataSet;
    DataSource1: TDataSource;
    DataSetProvider1: TDataSetProvider;
    ZQuery1: TZQuery;
    ZConnection1: TZConnection;
    Button1: TButton;
    Button2: TButton;
    DBNavigator1: TDBNavigator;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
begin

if ZConnection1.Connected then
begin
ZConnection1.Disconnect;
MessageBox(Handle, '�����������!',
PChar(Application.Title), MB_OK + MB_ICONINFORMATION + MB_TOPMOST);
Exit;
end;

{ ������������� }
ZConnection1.HostName := 'localhost';
ZConnection1.Port := 3306;
ZConnection1.Protocol := 'mysql';
ZConnection1.User := 'delphi';
ZConnection1.Password := 'delphi';
ZConnection1.Database := 'pogran';

{ ����������� }
try
ZConnection1.Connect;
MessageBox(Handle, '����������� ��������� �������!',
PChar(Application.Title), MB_OK + MB_ICONINFORMATION + MB_TOPMOST);
except
MessageBox(Handle, '����������� ����������� ��������!',
PChar(Application.Title), MB_OK + MB_ICONSTOP + MB_TOPMOST);
end;

end;

procedure TForm2.Button2Click(Sender: TObject);
begin

{ ����� (����� ����� ��������� � ���������) }
ZQuery1.Connection := ZConnection1;
DataSource1.DataSet := ZQuery1;
DBGrid1.DataSource := DataSource1;
DBNavigator1.DataSource := DataSource1;

{ ������������� }
ZConnection1.HostName := 'localhost';
ZConnection1.Port := 3306;
ZConnection1.Protocol := 'mysql';
ZConnection1.Database := 'pogran';
ZConnection1.User	 := 'delphi';
ZConnection1.Password := 'delphi';

{ ��������� }
ZConnection1.Connect;
ZQuery1.SQL.Text := 'SELECT * FROM people, auto, osnova, kpp';
ZQuery1.Active := True;

end;

procedure TForm2.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin

begin
if Assigned(Column) then
begin
DBGrid1.Canvas.FillRect(Rect);
DBGrid1.Canvas.TextRect(Rect, Rect.Left, Rect.Top, ' '+Column.Field.AsString);
end;
end;

end;

end.



